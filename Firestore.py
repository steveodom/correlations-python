from google.cloud import firestore


class Firestore(object):
    def __init__(self, batch = True):
        db = firestore.Client.from_service_account_json('./trading-v2-64c7b7f0a13e.json')
        self.ref = db.collection('correlations')
        if batch:
          self.batch = db.batch()

    def fetch_by_ticker(self, ticker):
        return self.ref.document(ticker).get().to_dict()

    def save_as_batch(self, ticker, data):
        doc_ref = self.ref.document(ticker)
        self.batch.set(doc_ref, data)

    def commit_batch(self):
        self.batch.commit()




d = Firestore().fetch_by_ticker('msft_2')
print(d)