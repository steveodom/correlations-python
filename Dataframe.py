import pandas as pd 
from Quotes import Quotes

class Dataframe(object):
    def __init__(self, ticker):
        self.ticker = ticker

    def populate_dataframe(self):
      res = Quotes(self.ticker).fetch('1d')
      if res:
        print('success')
        return pd.DataFrame(res)
      else:
        return None

    def convert(self):
        self.df.loc[:,'open'] = self.df['1. open'].astype(float)
        #self.df.loc[:,'high'] = self.df['2. high'].astype(float)
        #self.df.loc[:,'low'] = self.df['3. low'].astype(float)
        self.df.loc[:,'close'] = self.df['4. close'].astype(float)
        #self.df.loc[:,'volume'] = self.df['5. volume'].astype(int)
        self.df.drop(['1. open', '2. high', '3. low', '4. close', '5. volume'], axis=1)

    def normalize(self):
        #self.df["H/O"] = self.df["high"]/self.df["open"]
        #self.df["L/O"] = self.df["low"]/self.df["open"]
        self.df["C/O"] = self.df["close"]/self.df["open"]

    def build(self):
      self.df = self.populate_dataframe()
      if not self.df.empty:
        self.df = self.df.transpose()
        self.convert()
        self.normalize()
        return self.df


