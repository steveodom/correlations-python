import requests
# from s3 import S3

class Quotes(object):
    def __init__(self, ticker):
        self.ticker = ticker
        self.av_key = "0WY3"

    def alphavantage_params(self, period):
        output_size = 'full'
        endpoint = "TIME_SERIES_INTRADAY"
        key = "Time Series (5min)"

        if period == '3y' or period == '1m' or period == '1d' or period == '1m':
          endpoint = "TIME_SERIES_DAILY"
          key = "Time Series (Daily)"

        if period == '1d' or period == '1m':
          output_size = 'compact'
      
        path = "query?function={0}&symbol={1}&interval=5min&apikey={2}&outputsize={3}".format(endpoint, self.ticker, self.av_key, output_size)
        return path, key
       
    def fetch(self, period = "5min"):
        print('fetching...', self.ticker)
        path, key = self.alphavantage_params(period)
        url = 'https://www.alphavantage.co/{0}'.format(path)

        req = requests.get(url)
        #print(req.status_code, 'status_code')
        res = req.json()
        if key in res:
          return res[key]
        else:
          print('does not have ke')
          return []
    
    # def fetch_cached(self, quote_count):
    #     frame_size_in_minutes = 5
    #     return S3(quote_count, frame_size_in_minutes).fetch(self.ticker)