import numpy as np

from Dataframe import Dataframe
from Firestore import Firestore

import scipy.stats
import scipy.cluster.vq
from scipy.stats import linregress

number_of_groups_to_use = 7
#tickers = ['AAPL','MSFT','FB','CMCSA','INTC','CSCO','GILD','SBUX','TXN','MDLZ','ADBE','NVDA','FOX','FOXA','ADP','YHOO','AMAT','ATVI','EBAY','MU','ADI','DISH','INCY','EA','IMO','SIRI','LBTYK','AAL','MYL','PAYX','ADSK','SYMC','FITB','DLTR','SWKS','XLNX','HBAN','CA','STX','LNG','NTAP','DISCA','DISCK','CHRW','JBHT','EXPD','AKAM','ETFC','OTEX','SGEN','GT','MAT','FLEX','CDNS','STLD','ZION','ARCC','NWSA','NWS','MRVL']
tickers = ['AAPL','MSFT','FB']
#start = datetime.datetime(2017, 1, 1)
#end = datetime.datetime.now()
data = {}
quotes = {}
#rvalues = {}
#group_by_ticker = {}

ticker_count = len(tickers)
feature_matrix = np.zeros([ticker_count, ticker_count])

sorted_tickers = sorted(tickers)

for ticker in sorted_tickers:
    print(ticker)
    quote = Dataframe(ticker).build()
    data[ticker] = quote
    quotes[ticker] = {
        "quotes": quote['close'].values.tolist(),
        "rvalues": {},
        "group": None
    }
    #rvalues[ticker] = {}


for i, ticker in enumerate(sorted_tickers):
    quote = data[ticker]
    for j, target in enumerate(sorted_tickers):
        if ticker == target:
            quotes[ticker]["rvalues"][target] = 1
            continue
        else:
            target_quote = data[target]
        rvalue = linregress(quote["C/O"], target_quote["C/O"]).rvalue
        quotes[ticker]["rvalues"][target] = rvalue
        feature_matrix[i][j] = rvalue

#print(rvalues['FB'])
#print(quotes)
z = []
for ticker in sorted_tickers:
    target = quotes[ticker]["rvalues"]
    z.append(list(target.values()))

# cluster using k-means

W = scipy.cluster.vq.whiten(feature_matrix)
K = scipy.cluster.vq.kmeans(W, number_of_groups_to_use)
VQ = scipy.cluster.vq.vq(W, K[0])
groups = VQ[0]
# print(rvalues)
groups_lists = {}
for group, ticker in zip(groups, sorted_tickers):
    safe_group = group.item() # group is a numpy int64. item() converts it to native python int
    if safe_group not in groups_lists:
        groups_lists[safe_group] = []
    groups_lists[safe_group].append(ticker)
    quotes[ticker]["group"] = safe_group


fs = Firestore()

for ticker in quotes:
    data = quotes[ticker]
    fs.save_as_batch(ticker, data)

fs.commit_batch()